<?php

namespace SeeMoreMusic\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SeeMoreMusicUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
