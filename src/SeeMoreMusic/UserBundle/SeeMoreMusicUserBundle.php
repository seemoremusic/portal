<?php

namespace SeeMoreMusic\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SeeMoreMusicUserBundle extends Bundle
{
    /**
     * Extend the FOSUserBundle
     * @return string
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
